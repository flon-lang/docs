# FLON: Function-Like Object Notation

## Index

- [Introduction](#introduction)
- [Specification](#specification)
  - [Reserved characters](#reserved-characters)
  - [String](#string)
  - [Basket](#basket)
- Miscellaneous
  - [History](./history.md)
  - [Evaluation](./evaluation.md)

## Introduction

Common formats that many applications use for writing and storing configuration files are *JSON* and *YAML*. On top of the basic data types such as *string, integer, boolean, etc.*, formats like these use two virtually standard types of collections, the *list* and the *dictionary*.  A list is an ordered sequence of elements. A dictionary is a look-up table of key-value pairs. Along with the basic data types, this will cover most situations for which you need human-readable configuration.

In order to configure a flexible functional structure in conventional formats, you need to jump through hoops and invent a complicated mess of hierarchies. How do we represent procedures in a more approachable way, given that general purpose configuration formats are not quite suitable, and programming languages are too complex? I went about solving this by designing a new format, one that imitates function calls but without the need for any programming per se.

## Specification

**FLON**, or **FuLiON** 🦁, stands for **Function-Like Object Notation**. It is intended to make it easier for humans to note down and read functional structures by imitating how function calls in programming languages typically are written.

There are four tokens in total: *open parenthesis, closed parenthesis, equal sign for key-value pairs, and string.* These make up two data types in total: *strings* and *baskets*, the basket being the sole collection type of the language.

### Reserved characters

These characters will be prioritised unless escaped with a backslash.

- **parentheses**: `(` and `)` for opening and closing baskets.
- **delimiter**: `,` for separating values in a basket.
- **key-value assignor**: `=` for creating a key-value pair in a basket.

### String

A string is anything that isn't a parenthesis, delimiter, or key-value assignor. As in other languages, a string can be defined by a starting and ending quotation mark. And with any quotation marks within, you'll need to escape them using a backslash. Like YAML, you also have the option of not using quotation marks, but any reserved character you want to include in the string, you'll need to escape.

```flon
"Hello, World!"
```

```flon
Doth mother know you weareth her drapes?
```

Quotation marks are included with the string so that you get to decide for yourself how to treat it. You may want to distinguish `fruit` from `"fruit"`, as a variable in your program and not a conventional string.

#### Multiline

Line breaks are allowed in strings, both quoted and not. Whitespace is removed as in XML; lines are stripped and concatenated using singular spaces. To in stead start a new line, insert a `\n`.

```flon
Yar har, fiddle de dee.\n
Being a pirate is alright to be.\n
Do what you want 'cause a pirate is free.\n
You are a pirate!
```

### Basket

Basket is a multitypal collection. It can hold both positional values and keyword values. These are separated by commas\*. You access its positional values using integers, as you would a list, and its keyword values using strings\*\*, as you would a dictionary. A basket can also be named.

\* _Trailing commas are permitted._

\*\* Keys can also be baskets. To index a keyword value that has a basket as key, you use a basket of the same shape and contents, as you would with namedtuples for keys in Python._

#### Aliases

Depending on a basket's qualities, you might refer to it by a particular name. There are *lists*, *dictionaries*, *baskets*, and *functions*. In the end, they're all *baskets*. These aliases are superficial and meant to help convey what kind of data they hold.

As there is an alias also called *basket*, I'll be referring to the actual data type as *collection*.

#### List

A *list* is an **unnamed** collection with **only positional values**.

```flon
(Spoon, Bottle, Glass)
```

#### Dictionary

A *dictionary* is an **unnamed** collection with **only keyword values**.

```flon
(
  name = Jon,
  age = 23,
  job = unemployed,
)
```

#### Basket

A *basket* is either an **empty** collection, e.g. `()`, or an **unnamed** collection with **both positional and keyword values**.

```flon
(Pidgeon, 73, cheese=true)
```

#### Function

A *function* is a **named** *list*, *dictionary*, or *basket*. The name of the function is accessed as an attribute of the object, e.g. `the_function.name`. You name a function by placing a string in front of it.

```flon
do_a_flip(128, 2, voltz=1)
```

Names are just strings, so they follow the exact same rules as strings in other places. The following would be perfectly correct, too.

```flon
Do a flip! (128, 2, voltz=1)
```

I don't know why you would want to do it, but the following would be syntactically correct. However, I strongly question the ethical implications.

```flon
According to all known laws of aviation, there is no way that a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee\, of course\, flies anyway because bees don't care what humans think is impossible. ()
```