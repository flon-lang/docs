# FLON: Function-Like Object Notation

## Evaluation

There are many situations in which you need to be able to configure what procedures are executed and how, where *YAML* or *JSON* would do perfectly fine. That would be when you know the structure ahead of time and therefore can parse that specifically for your application. Where that falls short is when the hierarchy of procedures and where outputs are funneled are unknown, and this is where *FLON* would be used.

### Examples

#### A neural network for generating text

This network structure was borrowed from [a PyTorch tutorial](https://pytorch.org/tutorials/intermediate/char_rnn_generation_tutorial.html#creating-the-network) on the subject.

`capture` means to extract the output of a layer and store it in the given field.

```flon
RNN (category, character, hidden=zeros(1, 128)) = (

	i2o = Linear ((category, character, hidden)),
	i2h = Linear ((category, character, hidden)),
	
	capture(i2h, hidden),

	o2o = Linear ((i2o, hidden)),
	dpt = Dropout (o2o, 0.2),
	smx = Softmax (dpt, dim=1),
	
	capture(smx, character),
)
```

*- looks far more elegant than a whole block of -*

```yaml
RNN:
  inputs: [category, character, hidden]

  defaults:
    hidden:
      type: zeros
      size: [1, 128]
  
  graph:
    i2o:
      type: linear
      input: [category, character, hidden]
    
    i2h:
      type: linear
      input: [category, character, hidden]
      
    hidden:
      type: capture
      layer: i2h
      
    o2o:
      type: linear
      input: [i2o, hidden]
      
    dpt:
      type: dropout
      p: 0.2
      input: o2o
    
    smx:
      type: softmax
      dim: 1
      input: dpt

    character:
      type: capture
      layer: smx

  output: character
```

## Other work

During my later exploration of this subject, I discovered something similar called [Rust Object Notation](https://github.com/rson-rs/rson). It works off similar ideas but is much more confined in how it can be used because of its defined data types.
